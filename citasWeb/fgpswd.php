<?php
$codigoError ="";
session_start();
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">

    <title>CitasWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
<script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>

    <style>
/* Style all input fields */
input {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
}

/* Style the submit button */
input[type=submit] {
    background-color: #4CAF50;
    color: white;
}


</style>

  </head>

  <body>

<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Row -->
            
        <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/fgpswdfun.php"';?> method="post">

            <div class="form-title-row">
                <h2>Restablecimiento de Password</h2>
            </div>
                        <?php
                        //print_r($_SESSION['codigoError']);
                        print_r($codigoError);
                        ?>
                        <br>
    <h1>Favor Ingrese su usuario</h1>

			<div class="form-row">
                <label>
                    <span>Usuario</span>
                    <input type="text" name="user" placeholder="Usuario" required/>
                </label>
      </div>
      <p>En un momento recibira un correo electronico al e-mail que ingreso al registrarse con un password temporal con la que podrá ingresar a su cuenta y establecer un nuevo password</p>
            <div class="form-row">
                <button type="submit">Enviar</button>
            </div>
                    <p align="center"><a id="regresar-login" href="index.php">REGRESAR</a></p>
        </form>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>
