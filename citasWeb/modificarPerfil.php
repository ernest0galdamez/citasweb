<?php
$codigoError ="";
session_start();
//if(!empty($_SESSION['codigoError'])){
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}
$jsonData = $_SESSION['vsConsulta']; 
$jsonDataObject = json_decode($jsonData);
$_SESSION['usuario'] = $jsonDataObject->Usuario;
if(!isset($_SESSION['vsJsonAgencias']))
{
  header("location:index");
}
 if(isset($_SESSION['Flagpswd']))
  {
    if($_SESSION['Flagpswd']=="1"){
    header("location:fpswch");
    }
  }
//
//Expire the session if user is inactive for 30
//minutes or more.
$expireAfter = 5;
 
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){
    
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];
    
    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;
    
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        session_unset();
        session_destroy();
        header("location:index");
    }
    
}

//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">

    <title>CitasWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
<script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>

  </head>

  <body>
   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"';?>>Inicio
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/gestAgencia"';?>>Agendar Cita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/historialfunc.php"';?>>Historial de Citas</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/consultafunc.php"';?>>Modificar Perfil</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/pswch.php"';?>>Cambiar Password</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?> >Cerrar Sesión</a>
            </li>
          </ul>
        </div> 
      </div>
    </nav>


    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Row -->
            
        <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/modificarfun.php"';?> method="post">
			
            <div class="form-title-row">
                <h1>Actualización de Datos</h1>
                                <center>
                        <?php
                        //print_r($_SESSION['codigoError']);
                        print_r($codigoError);
                        ?>
                        <br>
      </center><br/>
            </div>

            <div class="form-row">
                <label>
                    <span>Nombre</span>
                    <input type="text" name="nombre" <?php $jsonData = $_SESSION['vsConsulta']; 
					$jsonDataObject = json_decode($jsonData); echo"value='".$jsonDataObject->Nombre."'"; ?> required/>
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Tipo Documento</span>
                    <?php
                    
                    echo '<select name="tip_doc">';
                    $jsonData = file_get_contents('http://'.$_SESSION['vsVelneo'].'/web/tipdoc?'); 
                    $jsonDataObject = json_decode($jsonData);

                   foreach($jsonDataObject->TiposDocs as $option){
                   echo '<option value=' . $option->ID . '>' . $option->Nombre . '</option>';                       
                    }

                    echo '</select>';
                    ?>
                </label>
            </div>

			<div class="form-row">
                <label>
                    <span>No Documento</span>
                    <input type="text" name="doc" <?php $jsonData = $_SESSION['vsConsulta']; 
					$jsonDataObject = json_decode($jsonData); echo"value='".$jsonDataObject->DocID."'"; ?> required/>
                </label>
            </div>

			<div class="form-row">
                <label>
                    <span>Usuario</span>
                    <input type="text" name="user" <?php $jsonData = $_SESSION['vsConsulta']; 
					$jsonDataObject = json_decode($jsonData); echo"value='".$jsonDataObject->Usuario."'"; ?> readonly required/>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Correo</span>
                    <input type="email" name="diremail" id="diremail" <?php $jsonData = $_SESSION['vsConsulta']; 
					$jsonDataObject = json_decode($jsonData); echo"value='".$jsonDataObject->DirEmail."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Telefono</span>
                    <input type="text"  name="tel" id="tel" placeholder="9999-9999" <?php $jsonData = $_SESSION['vsConsulta']; 
					$jsonDataObject = json_decode($jsonData); echo"value='".$jsonDataObject->Telefono."'"; ?> required pattern="[0-9]{8}" />
                </label>
            </div>


              <p align="center"></p>
              <div class="form-row">  
				    <button type="submit" class="btn btn-danger">Actualizar Datos</button>
			</div>
      <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu.php"><img alt="SALIR" height="42" src="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/regresar.png"" width="142"></a>';?>

                    
        </form>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>
