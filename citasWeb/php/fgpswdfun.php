<?php
session_start(); // start up your PHP session! 
//extract data from the post
//set POST variables
$fields_string = "";
$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/GENPSW?';
//Randomize Password 
$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
$passarray = array(); //remember to declare $passarray as an array
$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $passarray[] = $alphabet[$n];
}
//PASSWORD TO SEND IN EMAIL
$pswp = implode($passarray);
//$pass=md5($_POST['psw']);
//ENCRYP PASSWORD TO DATABASE
$psw          = hash('sha256', $pswp);

if (isset($_POST['user']))
{
  $user = $_POST['user'];
}




$fields        = array(
				'user' => urlencode($user),
				'psw' => urlencode($psw),
				'pswp' => urlencode($pswp),
);

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);

//$json = json_encode($result);
//print_r($result);
$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/fgpswd');
				die();
} else {
				//$_SESSION['jsonVelneo'] = $result; // store session data    
				//print_r ($_SESSION['jsonVelneo']);
				echo "<script language='javascript'>";
				echo "alert('USUARIO REGISTRADO CORRECTAMENTE')";
				echo "</script>";
				$_SESSION['codigoError'] = $decoded->Descripcion;
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/index');
				die();
}

?>