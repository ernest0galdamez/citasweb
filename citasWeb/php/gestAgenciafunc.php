<?php
session_start(); // start up your PHP session! 
//extract data from the post
//set POST variables
//is set the post variable
if (isset($_POST['fecha']))
{
  $fecha = $_POST['fecha'];
}


$fields_string = "";
$url           = "";
$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/gestores?';
$jsonAgencias  = $_SESSION['vsJsonAgencias'];
$jsondecode    = json_decode($jsonAgencias);
$cliente       = $jsondecode->id;
$fecha         = str_replace("/", "", $fecha);
$fecha2        = $fecha;

//is set the post variable codgestion
if (isset($_POST['codgestion']))
{
  $codgestion = $_POST['codgestion'];
}

//is set the post variable agencia
if (isset($_POST['agencia']))
{
  $agencia = $_POST['agencia'];
}



$fields = array(
				'cliente' => urlencode($cliente),
				'codgestion' => urlencode($codgestion),
				'agencia' => urlencode($agencia),
				'fecha' => urlencode($fecha)
);

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

//$json = json_encode($result);
//print_r($result);
$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/gestAgencia');
				die();
} else {
				$_SESSION['vsJsonHoras']  = $result; // store session data   
				$_SESSION['vsCodgestion'] = $codgestion;
				$_SESSION['vsAgencia']    = $agencia;
				$_SESSION['vsFecha']      = $fecha;
				$_SESSION['vsFecha2']     = $fecha2;
				$_SESSION['codigoError']  = "";
				//print_r ($_SESSION['jsonVelneo']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/gestorhora');
				die();
}

?>