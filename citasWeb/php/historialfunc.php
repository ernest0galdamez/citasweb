<?php
session_start(); // start up your PHP session! 
$fields_string = "";
$jsonAgencias  = $_SESSION['vsJsonAgencias'];
$jsondecode    = json_decode($jsonAgencias);
$cliente       = $jsondecode->id;
//extract data from the post
//set POST variables
$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/historial?';
$fields        = array(
				'cliente' => urlencode($cliente)
);


//print_r($fields)

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

//$json = json_encode($result);
//print_r($result);
$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/menu');
				die();
} else {
				$_SESSION['jsonHistorial'] = $result; // store session data    
				//print_r ($_SESSION['jsonVelneo']);
				$_SESSION['codigoError']   = $decoded->Descripcion;
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/historialCita');
				die();
}
?>