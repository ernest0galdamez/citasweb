<?php
session_start(); // start up your PHP session! 
//extract data from the post
//set POST variables
$fields_string = "";
$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/reguser?';
//$pass=md5($_POST['psw']);
//is set nombre
if (isset($_POST['nombre']))
{
  $nombre = $_POST['nombre'];
}

//is set doc
if (isset($_POST['doc']))
{
  $doc = $_POST['doc'];
}

//is set tel
if (isset($_POST['tel']))
{
  $tel = $_POST['tel'];
}

//is set tip_doc
if (isset($_POST['tip_doc']))
{
  $tip_doc = $_POST['tip_doc'];
}

//is set user
if (isset($_POST['user']))
{
  $user = $_POST['user'];
}

//is set diremail
if (isset($_POST['diremail']))
{
  $diremail = $_POST['diremail'];
}

//is set psw
if (isset($_POST['psw']))
{
  $psw = $_POST['psw'];
}


$pass          = hash('sha256', $psw);
$fields        = array(
				'nombre' => urlencode($nombre),
				'doc' => urlencode($doc),
				'tel' => urlencode($tel),
				'tip_doc' => urlencode($tip_doc),
				'user' => urlencode($user),
				'psw' => urlencode($pass),
				'diremail' => urlencode($diremail)
);

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);

//$json = json_encode($result);
//print_r($result);
$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/registro');
				die();
} else {
				//$_SESSION['jsonVelneo'] = $result; // store session data    
				//print_r ($_SESSION['jsonVelneo']);
				echo "<script language='javascript'>";
				echo "alert('USUARIO REGISTRADO CORRECTAMENTE')";
				echo "</script>";
				$_SESSION['codigoError'] = "";
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/index');
				die();
}

?>