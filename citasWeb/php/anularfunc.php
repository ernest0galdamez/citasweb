<?php
session_start(); // start up your PHP session! 
//extract data from the post
//set POST variables
$jsonAgencias  = $_SESSION['vsJsonAgencias'];
$jsondecode    = json_decode($jsonAgencias);
$cliente       = $jsondecode->id;
$fields_string = "";
$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/anulacita?';

if (isset($_POST['idCita']))
{
  $idCita = $_POST['idCita'];
}



$fields = array(
				'cliente' => urlencode($cliente),
				'NoCita' => urlencode($idCita)
);

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//echo $fields_string;
//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);

//print_r($result);
//$json = json_encode($result);

$decoded = json_decode($result);


if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/historialCita');
				die();
} else {
				$_SESSION['vsConsulta'] = $result; // store session data
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data  
				//print_r ($_SESSION['jsonVelneo']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/menu');
				die();
}

?>