<?php
session_start(); // start up your PHP session! 
$fields_string = "";
$jsonAgencias  = $_SESSION['vsJsonAgencias'];
$jsondecode    = json_decode($jsonAgencias);
$cliente       = $jsondecode->id;
//extract data from the post
//set POST variables
if (isset($_POST['s1']))
{
  $s1 = $_POST['s1'];
}
if (isset($_POST['s2']))
{
  $s2 = $_POST['s2'];
}



$url           = 'http://' . $_SESSION['vsVelneo'] . '/web/regcitas?';
$fields        = array(
				'cliente' => urlencode($cliente),
				'agencia' => urlencode($_SESSION['vsAgencia']),
				'tipoges' => urlencode($_SESSION['vsCodgestion']),
				'fecha' => urlencode($_SESSION['vsFecha']),
				'gestor' => urlencode($s1),
				'hora' => urlencode($s2),
				'funcion' => urlencode('A')
);


//print_r($fields)

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

//$json = json_encode($result);

$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data
				//print_r($decoded->Descripcion);
				//print_r ($_SESSION['codigoError']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/gestorhora');
				die();
} else {
				$_SESSION['jsonDetallecita'] = $result; // store session data 
				$_SESSION['codigoError']     = "";
				//print_r ($_SESSION['jsonVelneo']);
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/cita');
				die();
}
?>