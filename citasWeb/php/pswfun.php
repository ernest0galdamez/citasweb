<?php
session_start(); // start up your PHP session! 
//extract data from the post
//set POST variables
$jsonAgencias = $_SESSION['vsJsonAgencias'];
$jsondecode   = json_decode($jsonAgencias);

$user = $_SESSION['idUser'];

$fields_string = "";

//is set the post variable pswold	
if (isset($_POST['pswold']))
{
  $pswold = $_POST['pswold'];
}
//is set the post variable psw
if (isset($_POST['psw']))
{
  $psw = $_POST['psw'];
}


//$pswold=md5($_POST['pswold']);
//$psw=md5($_POST['psw']);
$pswold       = hash('sha256', $pswold);
$psw          = hash('sha256', $psw);
$jsonAgencias = $_SESSION['vsJsonAgencias'];
$url          = 'http://' . $_SESSION['vsVelneo'] . '/web/pswch?';
$fields       = array(
				'cliente' => urlencode($user),
				'pswold' => urlencode($pswold),
				'psw' => urlencode($psw)
);

//url-ify the data for the POST
foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);

//$json = json_encode($result);
//print_r($result);
$decoded = json_decode($result);

if ($decoded->Error != 200) {
				$_SESSION['codigoError'] = $decoded->Descripcion; // store session data		
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/pswch');
				die();
} else {
				$_SESSION['codigoError'] = '';
				session_unset();
				session_destroy();
				header('Location: http://' . $_SERVER['SERVER_NAME'] . '/citasweb/index');
				die();
}
?>