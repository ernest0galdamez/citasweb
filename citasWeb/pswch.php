<?php
$codigoError ="";
session_start();
//if(!empty($_SESSION['codigoError'])){
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}
$jsonData = $_SESSION['vsConsulta']; 
$jsonDataObject = json_decode($jsonData);
//Validar si existen las sesiones
if(!isset($_SESSION['vsJsonAgencias']))
  {
    header("location:index");
}
 if(isset($_SESSION['Flagpswd']))
  {
    if($_SESSION['Flagpswd']=="1"){
    header("location:fpswch");
    }
  }

//}
//Expire the session if user is inactive for 30
//minutes or more.
$expireAfter = 5;
 
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){
    
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];
    
    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;
    
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        session_unset();
        session_destroy();
        header("location:index");
    }
    
}

//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">

    <title>CitasWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
<script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>

    <style>
/* Style all input fields */
input {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
}

/* Style the submit button */
input[type=submit] {
    background-color: #4CAF50;
    color: white;
}

/* Style the container for inputs */
.container {

    padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
    display:none;
    color: #000;
    position: relative;
    padding: 5px;
    margin-top: 5px;
}

#message p {
    padding: 10px 35px;
    font-size: 15px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
    color: green;
}

.valid:before {
    position: relative;
    left: -5px;
    content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
    color: red;
}

.invalid:before {
    position: relative;
    left: -5px;
    content: "✖";
}
</style>

  </head>

  <body>

<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu.php"';?>>Inicio
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/gestAgencia"';?>>Agendar Cita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/historialfunc.php"';?>>Historial de Citas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/consultafunc.php"';?>>Modificar Perfil</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/pswch.php"';?>>Cambiar Password</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?> >Cerrar Sesión</a>
            </li>
          </ul>
        </div> 
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Row -->
            
        <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/pswfun.php"';?> method="post">

            <div class="form-title-row">
                <h1>Actualizacion de Password</h1>
            </div>
                        <?php
                        //print_r($_SESSION['codigoError']);
                        print_r( $codigoError);


                        ?>

      <div class="form-row">
      <label>
                    <span for="pswold">Password Actual</span>
                    <input type="password" name="pswold" id="pswold" placeholder="Password"/>
      </label>
      </div>
			<div class="form-row">
                <label>
                    <span for="psw">Password Nueva</span>
                    <input type="password" name="psw" id="psw" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="El Password no cumple los requerimientos."/>
                </label>
            </div>
              <div id="message" >
                    <h4>El Password cumplir las siguientes condiciones:</h4>
                    <p id="length" class="invalid">Minimo <b><u>8</u> caracteres</b></p>
                    <p id="letter" class="invalid"><u>UNA</u> <b>letra</b> Minuscula</p>
                    <p id="capital" class="invalid"><u>UNA</u> <b>letra</b> Mayuscula</p>
                    <p id="number" class="invalid"><u>UN</u> <b>numero</b></p>
                    <p id="specialchar" class="invalid"><u>UNO</u> <b>de los Caracteres siguientes: </b>-+!¡”¿?,.;:*/%&()’$#</p>
              </div>
			<div class="form-row">
                <label>
                    <span>Confirmar Password</span>
                    <input type="password" name="psw2" id="confirm_password" placeholder="Confirmar Password" required/>
                    <center><h5 style="text-align: right" id='samepsw'></h5></center>
                </label>
            </div>

            <div class="form-row">
                <button type="submit">Actualizar Password</button>
            </div>
                  <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu.php"><img alt="SALIR" height="42" src="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/regresar.png"" width="142"></a>';?>
        </form>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		    var password = document.getElementById("psw")
	  , confirm_password = document.getElementById("confirm_password");

		function validatePassword(){
		  if(password.value != confirm_password.value) {
			confirm_password.setCustomValidity("Las password NO son igual");
		  } else {
			confirm_password.setCustomValidity('');
		  }
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;

$('#psw, #confirm_password').on('keyup', function () {
    if ($('#psw').val() == $('#confirm_password').val()) {
        $('#samepsw').html('Los password coinciden ✔').css('color', 'green');
    } else 
        $('#samepsw').html('Los password NO coinciden ✖').css('color', 'red');
});

	</script>

<script>
var myInput = document.getElementById("psw");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var spechar = document.getElementById('specialchar')

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }

    // Validate numbers
  var specialcharacters = /[-+!¡”¿?,.;:*/%&()’$#]/g;
  if(myInput.value.match(specialcharacters)) {  
    specialchar.classList.remove("invalid");
    specialchar.classList.add("valid");
  } else {
    specialchar.classList.remove("valid");
    specialchar.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
  </body>

</html>
