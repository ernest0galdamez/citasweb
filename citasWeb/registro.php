<?php
$codigoError ="";
session_start();
//if(!empty($_SESSION['codigoError'])){
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}

//}	
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">

    <title>CitasWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
<script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>

    <style>
/* Style all input fields */
input {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
}

/* Style the submit button */
input[type=submit] {
    background-color: #4CAF50;
    color: white;
}


/* Style the container for inputs */
.container {

    padding: 20px;
}


/* The message box is shown when the user clicks on the password field */
#message {
    display:none;
    color: #000;
    position: relative;
    padding: 5px;
    margin-top: 5px;
}

#message p {
    padding: 0px 40px !important;
    text-align: left;
    font-size: 15px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
    color: green;
}

.valid:before {
    position: relative;
    left: -5px;
    content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
    color: red;
}

.invalid:before {
    position: relative;
    left: -5px;
    content: "✖";
}
</style>

  </head>

  <body>

<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Row -->
            
        <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/registrofunc.php"';?> method="post">
          <bn><br>
            <div class="form-title-row">
                <h1>Registro de Datos</h1>
            </div>
 <?php
                        //print_r($_SESSION['codigoError']);
                        print_r($codigoError);
                        ?>
            <div class="form-row">
                <label>
                    <span>Nombre</span>
                    <input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" oninvalid="this.setCustomValidity('Campo requerido, debe llenarlo')"
 oninput="setCustomValidity('')"  required/>
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Tipo Documento</span>
                    <?php
                    
                    echo '<select name="tip_doc">';
                    $jsonData = file_get_contents('http://'.$_SESSION['vsVelneo'].'/web/tipdoc?'); 
                    $jsonDataObject = json_decode($jsonData);

                   foreach($jsonDataObject->TiposDocs as $option){
                   echo '<option value=' . $option->ID . '>' . $option->Nombre . '</option>';                       
                    }

                    echo '</select>';
                    ?>
                </label>
            </div>

			<div class="form-row">
                <label>
                    <span>No Documento</span>
                    <input type="text" name="doc" placeholder="No Documento"  oninvalid="this.setCustomValidity('Campo requerido, debe llenarlo')"
 oninput="setCustomValidity('')"   required/>
                </label>
            </div>

			<div class="form-row">
                <label>
                    <span>Usuario</span>
                    <input type="text" name="user" placeholder="Usuario"  oninvalid="this.setCustomValidity('Campo requerido, debe llenarlo')"
 oninput="setCustomValidity('')" required/>
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span for="psw">Password</span>
                    <input type="password" name="psw" id="psw" autocomplete="off" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="El Password no cumple los requerimientos."/>
                </label>
            </div>
             <div id="message" >
                    <h4>El Password cumplir las siguientes condiciones:</h4>
                    <p id="length" class="invalid">Minimo <b><u>8</u> caracteres</b></p>
                    <p id="letter" class="invalid"><u>UNA</u> <b>letra</b> Minuscula</p>
                    <p id="capital" class="invalid"><u>UNA</u> <b>letra</b> Mayuscula</p>
                    <p id="number" class="invalid"><u>UN</u> <b>numero</b></p>
                    <p id="specialchar" class="invalid"><u>UNO</u> <b>de los Caracteres siguientes: </b>-+!¡”¿?,.;:*/%&()’$#</p>
              </div>
			<div class="form-row">
                <label>
                    <span>Confirmar Password</span>
                    <input type="password" name="psw2" id="confirm_password" placeholder="Confirmar Password" required/>
                    <center><h5 style="text-align: right" id='samepsw'></h5></center>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Correo</span>
                    <input type="email" name="diremail" id="diremail" maxLength="64" required placeholder="usuario@correo.com" title="El formato de la direccion de correo ingresada es INCORRECTO."/>
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Telefono</span>
                    <input type="text"  name="tel" id="tel"  placeholder="9999-9999" required pattern="[0-9]{8}" />
                </label>
            </div>

            <div class="form-row">
                <button type="submit">Enviar</button>
            </div>
                    <p align="center"><a id="regresar-login" href="index.php">REGRESAR</a></p>
        </form>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var password = document.getElementById("psw")
	  , confirm_password = document.getElementById("confirm_password");

		function validatePassword(){
		  if(password.value != confirm_password.value) {
			confirm_password.setCustomValidity("Las password NO son igual");

		  } else {
			confirm_password.setCustomValidity('');
		  }
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;

$('#psw, #confirm_password').on('keyup', function () {
    if ($('#psw').val() == $('#confirm_password').val()) {
        $('#samepsw').html('Los password coinciden ✔').css('color', 'green');
    } else 
        $('#samepsw').html('Los password NO coinciden ✖').css('color', 'red');
});

	</script>

<script>
var myInput = document.getElementById("psw");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var spechar = document.getElementById('specialchar')

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }

    // Validate numbers
  var specialcharacters = /[-+!¡”¿?,.;:*/%&()’$#]/g;
  if(myInput.value.match(specialcharacters)) {  
    specialchar.classList.remove("invalid");
    specialchar.classList.add("valid");
  } else {
    specialchar.classList.remove("valid");
    specialchar.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
  </body>

</html>
