﻿<?php
    session_start();
    //IP O NOMBRE DEL SERVER
    $localIP = getHostByName(getHostName());
    /*$serverApache=$localIP;*/
    $serverApache="192.168.0.16:80";
    $serverVelneo="192.168.0.100:8085";
    //$serverVelneo="192.168.0.80"
    $_SERVER['SERVER_NAME']="192.168.0.80:80";
    
    
    $codigoError ="";
    if(!empty($_SESSION['ErrorLogin'])){
    $codigoError = $_SESSION['ErrorLogin']; 
    $_SESSION['ErrorLogin']="";
    }
    if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}
     
    //Expire the session if user is inactive for 30
    //minutes or more.
    $expireAfter = 5;
     
    //Check to see if our "last action" session
    //variable has been set.
    if(isset($_SESSION['last_action'])){
        
        //Figure out how many seconds have passed
        //since the user was last active.
        $secondsInactive = time() - $_SESSION['last_action'];
        
        //Convert our minutes into seconds.
        $expireAfterSeconds = $expireAfter * 60;
        
        //Check to see if they have been inactive for too long.
        if($secondsInactive >= $expireAfterSeconds){
            //User has been inactive for too long.
            //Kill their session.
            session_unset();
            session_destroy();
        }
        
    }
    
    //Assign the current timestamp as the user's
    //latest activity
    $_SESSION['last_action'] = time();
     
    $_SERVER['SERVER_NAME']=$serverApache;
    $_SESSION['vsVelneo']=$serverVelneo;
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="SGLabz">
        <title>CitasWeb</title>
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/portfolio-item.css" rel="stylesheet">
        <link rel="stylesheet" href="css/form-basic.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/form.js"></script>
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
            </div>
        </nav>
        <!-- Page Content -->
        <div class="container">
            <!-- Portfolio Item Row -->
            <br/><br/><center><!--<img height="50" width="170" src="baclogo.png"" >--><a href="#"></a><img alt="LOGO" height="40" width="40" src="icon.png" >CitasWeb</a></center>
            <form class="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/loginfunc.php"';?>  id="form-basic" method="post">
                 
                <div class="form-title-row">
                   
                    <h1>Inicio de Sesión</h1>
                                <center>
                    <?php
                        //print_r($_SESSION['codigoError']);
                        //echo $_SERVER['SERVER_NAME'];
                        
                        print_r( $codigoError);
                        ?>
                </center>
                </div>

                <div class="form-row">
                    <label>
                    <span>Usuario</span>
                    <input type="text" name="user" id="user" placeholder="Usuario" required/>
                    </label>
                </div>
                <div class="form-row">
                    <label>
                    <span>Password</span>
                    <input type="password" id="psw" name="psw" placeholder="Password" required /> 
                    </label>
                </div>
                <div class="button-row">
                    <button type="submit">Enviar</button>
                </div>

                <br/>
                <p align="center">¿No tienes una cuenta? <a href="registro.php">REGISTRATE</a>.</p>
                <p align="center">¿Olvidaste tu password? <a href="fgpswd.php">OLVIDE PASSWORD</a>.</p>
            </form>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container -->
        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>
        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/popper/popper.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>