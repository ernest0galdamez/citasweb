<?php
  //Habilitar las sesiones
  $codigoError ="";
  session_start();
  
  //Validar si existen las sesiones
  if(!isset($_SESSION['vsJsonAgencias']))
  {
    header("location:index");
  }
  if(isset($_SESSION['Flagpswd']))
  {
    if($_SESSION['Flagpswd']=="1"){
    header("location:fpswch");
    }
  }
  
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}

  //Expire the session if user is inactive for 30
  //minutes or more.
  $expireAfter = 5;
   
  //Check to see if our "last action" session
  //variable has been set.
  if(isset($_SESSION['last_action'])){
      
      //Figure out how many seconds have passed
      //since the user was last active.
      $secondsInactive = time() - $_SESSION['last_action'];
      
      //Convert our minutes into seconds.
      $expireAfterSeconds = $expireAfter * 60;
      
      //Check to see if they have been inactive for too long.
      if($secondsInactive >= $expireAfterSeconds){
          //User has been inactive for too long.
          //Kill their session.
          session_unset();
          session_destroy();
          header("location:index");
      }
      
  }
  
  //Assign the current timestamp as the user's
  //latest activity
  $_SESSION['last_action'] = time();
  //}
  ?>
<!DOCTYPE html>
<link href="css/tabla.css" rel="stylesheet">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">
    <title>CitasWeb</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="js/form.js"></script>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"';?>>Inicio
              <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/gestAgencia"';?>>Agendar Cita</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/historialfunc.php"';?>>Historial de Citas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/consultafunc.php"';?>>Modificar Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/pswch.php"';?>>Cambiar Password</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?> >Cerrar Sesión</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
      <!-- Portfolio Item Row -->
            
      <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/anularfunc.php"';?> method="post" >
        <div class="form-title-row">
          <h1>Citas Programadas</h1>
        </div>
        <div class="form-row table table-responsive" style="overflow-x:auto;">
          <table id="table_tingkat_jual">
            <thead>
              <tr>
                <th>No. CITA</th>
                <th>AGENCIA</th>
                <th>GESTION</th>
                <th>FECHA</th>
                <!--<th>GESTOR</th>-->
                <th>HORA</th>
                <!--<th></th>-->
              </tr>
            </thead>
            <tbody>
              <?php
                $jsonData =$_SESSION['jsonHistorial']; 
                $jsonDataObject = json_decode($jsonData);
                foreach($jsonDataObject->Citas as $option)
                {
                echo '<tr >';
                      echo '<td id="'. $option->NoCita . '">'. $option->NoCita . '</td>';
                echo '<td>'. $option->Agencia . '</td>';
                echo '<td>'. $option->Gestion . '</td>';
                echo '<td>'. $option->Fecha . '</td>';
                //echo '<td>'. $option->Gestor . '</td>';
                echo '<td>'. $option->hora . '</td>';
                //echo '<td><a onclick="confirmar();" ><IMG SRC="delete.png" ALT="ELIMINAR" WIDTH=40 HEIGHT=40/></a></td>';
                echo '</tr>';
                }
                
                ?>
            </tbody>
          </table>
        </div>
        <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"><img alt="SALIR" height="42" src="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/regresar.png"" width="142"></a><br/>';?>
        <div class="form-row">
        </div>
        <center>¿Desea anular una cita?</center>
        <br/>
        <label>
        Seleccionar
        <?php
          echo '<select name="idCita">';
          $jsonData =$_SESSION['jsonHistorial']; 
          $jsonDataObject = json_decode($jsonData);
          
          foreach($jsonDataObject->Citas as $option){
              echo '<option value=' . $option->NoCita . '>' . 'Cita No.'. $option->NoCita . ' en Agencia '.  $option->Agencia  . '</option>';
             
          }
          
          echo '</select>';
          ?>
        </label>
        <br/><br/>
        <div class="form-row">
          <button type="submit" >ANULAR</button>
        </div>
      </form>
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
      <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="code.jquery.com/jquery-2.2.4.min.js"></script>
    <script type="text/javascript">
      function confirmar(){
        var mensaje = confirm('¿Está seguro de anular esta cita?');
        if (mensaje) {
          $('#table_tingkat_jual tr').click(function(e) {
            e.stopPropagation();
            var $this = $(this);
            var tdid = $this.find('td[id]').attr('id');       
            var obj = { "idCita": tdid };
             var jsonid = JSON.stringify(obj);
            $(function(){
                     $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "anularfunc.php",
                         data: jsonid,﻿
                         success: function (data) { alert(data) },
                         eror: function (data) { alert(data) }
                     });
         });
            
            
            //alert("TD ID " + tdid);
          });
          
          //window.location.href="http://localhost/citasweb/anularfunc.php";
        }
        else{
          window.location.href="http://localhost/citasweb/historialCita.php";
        }
      }
    </script>
  </body>
</html>