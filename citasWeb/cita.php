  <?php
//Habilitar las sesiones
$codigoError ="";
session_start();

//Validar si existen las sesiones
if(!isset($_SESSION['vsJsonAgencias']))
{
	header("location:index");
}
 if(isset($_SESSION['Flagpswd']))
  {
    if($_SESSION['Flagpswd']=="1"){
    header("location:fpswch");
    }
  }
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}

$jsonData = $_SESSION['jsonDetallecita']; 
$jsonDataObject = json_decode($jsonData);
//Expire the session if user is inactive for 30
//minutes or more.
$expireAfter = 5;
 
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){
    
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];
    
    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;
    
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        session_unset();
        session_destroy();
        header("location:index");
    }
    
}

//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">

    <title>CitasWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
<script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>
	<link href="css/pikaday.css" rel="stylesheet">
	
	<script src="js/jquery-1.12.4.js"></script>

<!-- date new-->
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="js/jquery-1.12.1.js"></script>
  </head>

  <body>
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
        <div class="button-row-salir">
      <div><a title="Cerrar Sesión" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?> ></a></div><br/><p>
    </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"';?>>Inicio</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/gestAgencia"';?>>Agendar Cita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/historialfunc.php"';?>>Historial de Citas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/consultafunc.php"';?>>Modificar Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?>>Cerrar Sesión</a>
            </li>
          </ul>
        </div> 
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">


      <!-- Portfolio Item Row -->
            
        <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"';?> method="post">

            <div class="form-title-row">
                <h1>Detalle de la Cita</h1>
                <center>
           <?php
                        //print_r($_SESSION['codigoError']);
                        print_r( $codigoError);
                        ?>
                        <br>
      </center><br/>
            </div>
			
			<div class="form-row">
                <label>
                    <span>No. Cita</span>
                    <input input type="text" name="nocita" disabled <?php echo"value='".$jsonDataObject->NoCita."'"; ?> />
                </label>
            </div>
                     
            <div class="form-row">
                <label>
                    <span>Agencia</span>
                    <input input type="text" name="fecha" disabled <?php echo"value='".$jsonDataObject->Agencia."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Gestión</span>
                    <input input type="text" name="gestion" disabled <?php echo"value='".$jsonDataObject->Gestion."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Fecha</span>
                    <input input type="text" name="fecha" disabled <?php echo "value='".$_SESSION['vsFecha2']."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Gestor</span>
                    <input input type="text" name="gestor" disabled <?php echo"value='".$jsonDataObject->Gestor."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <label>
                    <span>Hora</span>
                    <input input type="text" name="hora" disabled <?php echo"value='".$jsonDataObject->hora."'"; ?> />
                </label>
            </div>
			
			<div class="form-row">
                <button type="submit" >Aceptar</button>

            </div>
			
        </form>

      </div>
      <!-- /.row -->



    </div>
    <!-- /.container -->

<footer class="py-5 bg-dark">
            <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
            <!-- /.container -->
        </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="js/pikaday.js"></script>
	<script src="js/moment.js"></script>
	<!-- Calendario 
	<script>
		var picker = new Pikaday({
			field: document.getElementById('datepicker'),
			format: 'D MMM YYYY',
			onSelect: function() {
				console.log(this.getMoment().format('Do MMMM YYYY'));
			}
		});
	</script>
	<script>
		var picker = new Pikaday({ field: document.getElementById('datepicker') });
	</script>-->
 <script>
  $( function() {
    $( "#datepicker" ).datepicker();
     $.datepicker.regional['es'] = {
    closeText: "Cerrar",
    prevText: "&#x3C;Ant",
    nextText: "Sig&#x3E;",
    currentText: "Hoy",
    monthNames: [ "Enero","Febrero","Marzo","Abril","Mayo","Junio",
    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ],
    monthNamesShort: [ "ene","feb","mar","abr","may","jun",
    "jul","ago","sep","oct","nov","dic" ],
    dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
    dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
    dayNamesMin: [ "D","L","M","M","J","V","S" ],
    weekHeader: "Sm",
    dateFormat: "dd/mm/yy",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: "" };
 $.datepicker.setDefaults($.datepicker.regional['es']);
  } );
  </script>


    </body>
</html>

