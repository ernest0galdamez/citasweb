<?php
  //Habilitar las sesiones
  $codigoError ="";
  session_start();
  
  //Validar si existen las sesiones
  if(!isset($_SESSION['vsJsonAgencias']))
  {
    header("location:index");
  }
  if(isset($_SESSION['Flagpswd']))
  {
    if($_SESSION['Flagpswd']=="1"){
    header("location:fpswch");
    }
  }
  
if(isset($_SESSION['codigoError']))
  {
   $codigoError = $_SESSION['codigoError']; 
   $_SESSION['codigoError']="";
}

  //Expire the session if user is inactive for 30
  //minutes or more.
  $expireAfter = 5;
   
  //Check to see if our "last action" session
  //variable has been set.
  if(isset($_SESSION['last_action'])){
      
      //Figure out how many seconds have passed
      //since the user was last active.
      $secondsInactive = time() - $_SESSION['last_action'];
      
      //Convert our minutes into seconds.
      $expireAfterSeconds = $expireAfter * 60;
      
      //Check to see if they have been inactive for too long.
      if($secondsInactive >= $expireAfterSeconds){
          //User has been inactive for too long.
          //Kill their session.
          session_unset();
          session_destroy();
          header("location:index");
      }
      
  }
  //Assign the current timestamp as the user's
  //latest activity
  $_SESSION['last_action'] = time();
  //}
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="SGLabz">
    <title>CitasWeb</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/portfolio-item.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form-basic.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/form.js"></script>
    <link rel="stylesheet" href="js/jquery-ui.css" />
    <script src="js/jquery-1.9.1.js"></script>
    <script src="js/jquery-ui.js"></script>
  </head>
  <body onload = "loadMap()">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img height="50" style="margin: auto;" width="170" src="bacrojo.jpg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"';?>>Inicio
              <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/gestAgencia"';?>>Agendar Cita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/historialfunc.php"';?>>Historial de Citas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/consultafunc.php"';?>>Modificar Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo 'href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/pswch.php"';?>>Cambiar Password</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/salir.php"';?> >Cerrar Sesión</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
      <!-- Portfolio Item Row -->
            
      <form class="form-basic" id="form-basic" <?php echo 'action="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/gestAgenciafunc.php"';?> method="post">
        <div class="form-title-row">
          <h1>Seleccionar</h1>
           <center>
          <?php
            //print_r($_SESSION['codigoError']);
            print_r($codigoError);
            ?>
          <br>
        </div>
        <div class="form-row">
          <label>
          <span>Gestión</span>
          <?php
            echo '<select name="codgestion">';
            $jsonData =$_SESSION['vsJsonAgencias']; 
            $jsonDataObject = json_decode($jsonData);
            
            foreach($jsonDataObject->TipoGestion as $option){
                echo '<option value=' . $option->ID . '>' . $option->Nombre . '</option>';
               
            }
            
            echo '</select>';
            ?>
          </label>
        </div>
        <div class="form-row">
          <label>
          <span>Fecha</span>
          <input type="text" id="datepicker" name="fecha"  autocomplete="off" required/>
          </label>
        </div>
        <div class="form-row">
          <label>
          <span>Agencia</span>
          <?php
            echo '<select id="agencia" name="agencia">';
            $jsonData = $_SESSION['vsJsonAgencias']; 
            //$jsonData = file_get_contents('https://api.myjson.com/bins/t222l'); 
            $jsonDataObject = json_decode($jsonData);
            
            foreach($jsonDataObject->Agencias as $option){
                echo '<option value=' . $option->ID . '>' . $option->Nombre . '</option>';
            //CAPTURAR NOMBRE DE LA AGENCIA
            $_SESSION['vsNomagencia'] = $option->Nombre;
            }
            
            echo '</select>';
            ?>
          </label>
        </div>
        <h2>Agencias</h2>
        <?php
          //$jsonData = file_get_contents('https://api.myjson.com/bins/t222l'); 
          $jsonDataObject = json_decode($jsonData);
          ?>
        <center>
          <div id = "map" class="google-maps"></div>
          <script>
            // fake JSON call
            function getJSONMarkers() {
              var markers =  <?php echo $jsonData ?>;
              return markers;
            }
            
            function loadMap() {
              // Initialize Google Maps
              const mapOptions = {
                center:new google.maps.LatLng(13.706360,-89.212204),
                zoom: 10
              }
              infoWindow = new google.maps.InfoWindow;
            
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
            
                infoWindow.setPosition(pos);
                infoWindow.setContent('Usted esta aquí.');
                infoWindow.open(map);
                map.setCenter(pos);
              }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }
            
              const map = new google.maps.Map(document.getElementById("map"), mapOptions);
            
              // Load JSON Data
              const data = getJSONMarkers();
            
              // Initialize Google Markers
              for(agencia of data.Agencias) {
                  let marker = new google.maps.Marker({
                  id: agencia.ID,
                  map: map,
                  position: new google.maps.LatLng(agencia.Latitud, agencia.Longitud),
                  content: agencia.Nombre,
                  
                  })
            
              marker.info = new google.maps.InfoWindow({
                content: agencia.Nombre
              });
            
              google.maps.event.addListener(marker, 'click', function() {
                marker.info.open(map, marker);
                marker = this; 
                //alert('ID is :'+ this.id);
                //$(document).ready(function(){
                // edit $('#agencia option[value="' + this.id + '"]').prop("selected", true);
                $("#agencia").val(this.id);
              //});
              });
              }
            }
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD-h6xw5-2X2DdSmL93dQmrR7p63Q_uv5w"></script>
        </center>
        <div class="form-row">
          <button type="submit" >Consultar Horarios</button>
        </div>
        </center>
        <br/>
        <?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/citasweb/menu"><img alt="SALIR" height="42" src="http://'.$_SERVER['SERVER_NAME'].'/citasweb/php/regresar.png"" width="142"></a>';?>
      </form>
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div style="font-size: 12px" class="container">
                <p class="m-0 text-center text-white">Copyright&copy; CitasWeb 2017-2018</p>
                </br><p class="m-0 text-center text-white">Desarrrollado por <a href="http://www.conticsa.com">CONTICSA</a></p>
            </div>
      <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/pikaday.js"></script>
    <script src="js/moment.js"></script>
    <script>
      $.noConflict();
      $( function() {
      
        $( "#datepicker" ).datepicker();
         $.datepicker.regional['es'] = {
        closeText: "Cerrar",
        prevText: "&#x3C;Ant",
        nextText: "Sig&#x3E;",
        currentText: "Hoy",
        monthNames: [ "Enero","Febrero","Marzo","Abril","Mayo","Junio",
        "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ],
        monthNamesShort: [ "ene","feb","mar","abr","may","jun",
        "jul","ago","sep","oct","nov","dic" ],
        dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
        dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
        dayNamesMin: [ "D","L","M","M","J","V","S" ],
        weekHeader: "Sm",
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "" };
      $.datepicker.setDefaults($.datepicker.regional['es']);
      } );
    </script>
  </body>
</html>